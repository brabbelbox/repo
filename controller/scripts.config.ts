import type { DenonConfig } from "https://deno.land/x/denon/mod.ts"

const config: DenonConfig = {
  "scripts": {
    "start": {
      "cmd": "deno run --allow-net --allow-read --allow-write --allow-run --allow-env src/server.ts",
      "desc": "start server"
    },
  },
}

export default config
