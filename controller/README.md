# Brabbelbox v1 Controller Backend

## start
```
deno run --allow-net --allow-read --allow-write --allow-env src/server.ts 1111
```

or install [denon](https://github.com/denosaurs/denon) and run

```
denon start
```

for automatic restarts on code changes.

## endpoints
Data structure types are defined in `src/types.ts`

### `GET /api/state`
responds with the current full state as `APIState`

### `PATCH /api/preferences`
updates preferences like e.g. `enabledLangs` or `txBaseFreq`. Can also be used to apply all settings defined in the wizard or to import configs

### `PATCH /api/element/:id`
body should be `Partial<Element>`. updates an element, potentially triggering a rerouting when languages get changed or a element gets disabled/enabled.

```ts
Partial<Element> {
    langs: LanguageTuple[]
    provideGain: number
    acceptGain: number
    isOutputMuted: boolean
    txChannel: number
    isDisabled: boolean
}
```


### `GET /api/config`
shows the config as defined in `config.yaml`


## upcoming/planned endpoints

### `POST /api/element/:id/mixer_overrides`
accepts an object with the inputs and their volumes. this overrides whatever the automatically generated graph/mapping would define.
```json
{
    "stage": 0.2,
    "int4": 1
}
```

### `DELETE /api/element/:id/mixer_overrides`
removes any overrides defined for this element


<!--
### `GET /api/presets`

### `POST /api/presets`

### `DELETE /api/presets/:id` -->


## logging

you can configure the logging level by setting an environment variable called `BRABBEL_LOG_LEVEL` to `DEBUG|INFO|WARNING|ERROR|CRITICAL`
