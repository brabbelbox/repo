import { broadcast } from './api/routes/ws.ts'
import { config } from './config.ts'
import { getMixerHealth } from './hexmatrix.ts'
import { Status } from './types.ts'
import { isRunning as isGnuradioActive } from './gnuradio.ts'
import { sleep } from './utils.ts'
import { getLogger } from './logging.ts'

const log = getLogger('status')

async function getMemoryUsage() {
  const memInfo = new Deno.Command('cat', {
    args: ['/proc/meminfo'],
  })

  const memOutput = await memInfo.output()
  const memStr = new TextDecoder().decode(memOutput.stdout)
  let memFree = -1
  let memTotal = -1
  for (const line of memStr.split('\n')) {
    if (line.includes('MemTotal:')) {
      memTotal = Math.floor(
        Number(line.split(':')[1].slice(0, -3).trim()) / 1024,
      )
      continue
    }
    if (/(^MemFree|^Buffers|^Cached|^SReclaimable):/.test(line)) {
      memFree += parseInt(line.split(':')[1].slice(0, -3).trim())
      continue
    }
  }
  memFree = Math.ceil(memFree / 1024)
  return { memFree, memTotal }
}

async function getCpuUsage() {
  const prev = await getCpuStat()
  await sleep(1000)
  const cur = await getCpuStat()
  const utilization: number[] = []
  for (let i = 0; i < cur.length; i++) {
    const active = cur[i].active - prev[i].active
    const total = cur[i].total - prev[i].total
    utilization.push(Math.round(active / total * 100))
  }
  return utilization
}

async function getCpuStat() {
  const cpuInfo = new Deno.Command('cat', {
    args: ['/proc/stat'],
  })

  const cpuOutput = await cpuInfo.output()
  const cpuStr = new TextDecoder().decode(cpuOutput.stdout)
  const cpus: Array<{ id: string; total: number; active: number }> = []
  for (const line of cpuStr.split('\n')) {
    if (!line.includes('cpu')) continue
    const cpuStats = line.split(' ')
    if (cpuStats[0] === 'cpu') continue // only individual cores

    const [_id, user, nice, system, idle, iowait, _irq, softirq, steal] = cpuStats.map((c) =>
      Number(c)
    )
    const active = user + system + nice + softirq + steal
    const total = user + system + nice + softirq + steal + idle + iowait
    cpus.push({ id: cpuStats[0], active, total })
  }
  return cpus
}

async function isUsbPresent(vendorAndProductId: string) {
  const lsusb = new Deno.Command('lsusb', {
    args: ['-d', vendorAndProductId],
  })
  const lsusbOutput = await lsusb.output()
  return lsusbOutput.success
}

async function getStatus(): Promise<Status> {
  log.debug('check current status')
  const mixer = await getMixerHealth()
  return {
    mixerRunning: mixer.isRunning,
    mixerConnections: mixer.connectedPorts,
    audioInterfaceFound: await isUsbPresent(config.audioInterfaceUsb),
    radioFound: await isUsbPresent(config.radioUsb),
    radioActive: isGnuradioActive(),
    memFree: (await getMemoryUsage()).memFree,
    cpuUsage: await getCpuUsage(),
  }
}

const STATUS_UPDATE_INTERVAL = 2000

export function startStatusChecker() {
  setInterval(() => {
    // broadcast to all active websockets
    getStatus()
      .then((status) => broadcast('status', status))
      .catch(console.log)
  }, STATUS_UPDATE_INTERVAL)
}
