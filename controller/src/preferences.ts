import { loadPreset, savePreset } from './api/presets.ts'
import { apply as applyGraph } from './routing.ts'
import { Element, ElementType, Preferences } from './types.ts'
import * as gnuradio from './gnuradio.ts'
import { reloadMixerState } from './hexmatrix.ts'
import { broadcast } from './api/routes/ws.ts'
import { getLogger } from './logging.ts'

const log = getLogger('preferences')

const preferences: Preferences = {
  enabledLangs: ['de', 'en'],
  speakerLangs: [],
  elements: [
    {
      id: 'stage',
      type: ElementType.Stage,
      langs: [{ provides: 'de' }],
    },
  ],
  mixerOverrides: {},
  txBaseFreq: 90,
  outputActive: false,
}
function getElementsByType(type: ElementType) {
  return preferences.elements.filter((el) => el.type == type)
}

export function getPreferences(): Preferences {
  return self.structuredClone(preferences)
}

export function setPreferences(newPreferences: Preferences) {
  let gnuradioHasChanged = false
  if (
    'outputActive' in newPreferences && preferences.outputActive !== newPreferences.outputActive
  ) gnuradioHasChanged = true
  if ('txBaseFreq' in newPreferences && preferences.txBaseFreq !== newPreferences.txBaseFreq) {
    gnuradioHasChanged = true
  }
  const oldTxChannel = getElementsByType(ElementType.Transmitter).map((el) => el.txChannel)

  Object.assign(preferences, self.structuredClone(newPreferences))
  savePreferences()
  broadcast('prefs', getPreferences())
  triggerGraphUpdate()

  const newTxChannel = getElementsByType(ElementType.Transmitter).map((el) => el.txChannel)
  if (oldTxChannel.join(',') !== newTxChannel.join(',')) gnuradioHasChanged = true
  if (gnuradioHasChanged) applyGnuradioUpdate()
}

function applyGnuradioUpdate() {
  if (preferences.outputActive) {
    const transmitter = getElementsByType(ElementType.Transmitter)

    // get highest tx id
    let highestIndex = 0
    for (const tx of transmitter) {
      const index = parseInt(tx.id.substring(ElementType.Transmitter.length))
      if (index > highestIndex) highestIndex = index
    }

    // even when a tx id is missing due to deletion (e.g. tx1,tx3,tx4)
    // the gnuradio channels should still be 4 numbers so that
    // the pipewire channels/connections stay identical.
    // passing an empty string/null to the gnuradio script will create
    // a null sink for that audio channel
    const gnuradioChannelMapping: Array<number | null> = []
    for (let i = 1; i <= highestIndex; i++) {
      const tx = transmitter.find((t) => t.id === 'tx' + i)
      if (tx?.txChannel || tx?.txChannel === 0) gnuradioChannelMapping.push(tx?.txChannel)
      else gnuradioChannelMapping.push(null)
    }
    gnuradio.start(preferences.txBaseFreq, gnuradioChannelMapping)
  } else {
    gnuradio.stop()
  }
}

let saveDelay: number
function savePreferences() {
  // debounce saving
  clearTimeout(saveDelay)
  saveDelay = setTimeout(_savePreferences, 2 * 1000)
}

async function _savePreferences() {
  try {
    const data = { ...preferences } // clone
    delete data.outputActive
    await savePreset('_autosaved', 'Last Settings', data)
    log.debug('preferences saved')
  } catch (err) {
    log.error('error while saving preferences', err)
  }
}

export function updateElement(id: string, data: Partial<Element>) {
  log.debug(`updateElement(${id})`, data)
  const element = preferences.elements.find((e) => e.id === id)
  const prevTxChannel = element?.txChannel
  if (!element) throw new Error('element not found')

  Object.assign(element, data)
  savePreferences()
  broadcast('prefs', getPreferences())
  triggerGraphUpdate()
  if (element.txChannel !== prevTxChannel) applyGnuradioUpdate()
}

async function triggerGraphUpdate() {
  const prefs = getPreferences()
  await applyGraph({
    elements: prefs.elements,
    mixerOverrides: prefs.mixerOverrides,
  })
  reloadMixerState()
}

loadPreset('_autosaved').then((newPrefs) => {
  // on restart output should never be activated automatically
  newPrefs.outputActive = false
  setPreferences(newPrefs)
  log.info('auto saved settings loaded')
}).catch((err) => {
  log.error('error while loading auto saved settings', err)
})
