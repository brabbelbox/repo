import { config } from './config.ts'
import { path } from './deps.ts'
import { getLogger } from './logging.ts'

const log = getLogger('gnuradio')

const gnuradioPath = path.join(path.dirname(path.fromFileUrl(import.meta.url)), '../../gnuradio')

let p: Deno.ChildProcess | null = null

let _isRunning = false

export async function start(baseFreq: number, channels: Array<number | null>) {
  if (p) await stop()
  log.info('starting gnuradio python script...')
  p = (new Deno.Command('pw-jack', {
    args: [
      'python3',
      'brabbelbox_rf.py',

      // args
      baseFreq.toFixed(1),
      channels.map((c) => typeof c === 'number' ? c : '').join(','),
    ],
    env: {
      GR_CONF_AUDIO_AUDIO_MODULE: 'jack',
      PIPEWIRE_LATENCY: config.gnuradioPipewireLatency,
    },
    cwd: gnuradioPath,
  })).spawn()

  _isRunning = true
  p.output().then((output) => {
    _isRunning = false
    if (output.code !== 0) {
      log.error(`gnuradio exited with status code ${output.code}`)
    } else {
      log.info('gnuradio stopped')
    }
    // TODO: auto restart on failure?
  })
}

export async function stop() {
  if (!p || !_isRunning) return
  log.info('stopping gnuradio...')
  p.kill()
  await p.output()
  p = null
}

export function isRunning() {
  return _isRunning
}
