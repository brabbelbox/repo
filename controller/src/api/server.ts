import { OakApplication, oakCors } from '../deps.ts'
import { getLogger, oakLogger } from '../logging.ts'
import { startMixerPolling, watchMixerMeters } from '../hexmatrix.ts'
import { startStatusChecker } from '../status.ts'
import router from './routes/index.ts'

const log = getLogger('server')

export async function startApi(port: number) {
  const app = new OakApplication()

  const cors = oakCors()
  app.use(cors)
  app.use(oakLogger)

  app.use(async (ctx, next) => {
    try {
      await next()
    } catch (err) {
      ctx.response.status = err.status || 500
      ctx.response.body = err.message
    }
  })
  app.use(router.routes())
  app.use(router.allowedMethods())

  // app.use((ctx) => {
  //   ctx.response.body = 'Hello from your first oak server'
  // })

  log.info(`oak server running at http://localhost:${port}/`)

  startStatusChecker()
  startMixerPolling()
  watchMixerMeters()

  // await initState()
  await app.listen({ port })
}
