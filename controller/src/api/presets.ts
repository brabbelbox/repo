import { Preferences } from '../types.ts'
import { path, yamlParse, yamlStringify } from '../deps.ts'
import { getLogger } from '../logging.ts'

const log = getLogger('presets')

const DIRECTORY = path.join(path.dirname(path.fromFileUrl(import.meta.url)), '../../presets')

interface Preset {
  title: string
  prefs: Preferences
}

export async function savePreset(filename: string, title: string, prefs: Preferences) {
  log.debug('saving preset', filename)
  const file = path.join(DIRECTORY, filename + '.yml')

  const data: Preset = {
    title,
    prefs,
  }
  await Deno.writeTextFile(file, yamlStringify(data as any))
}

export async function loadPreset(filename: string): Promise<Preferences> {
  log.debug('loading preset', filename)
  const file = path.join(DIRECTORY, filename + '.yml')
  const data = yamlParse(await Deno.readTextFile(file)) as Preset
  return data.prefs
}
