// deno-lint-ignore-file no-explicit-any
import { OakRouter } from '../../deps.ts'
import { getMixerMatrix } from '../../hexmatrix.ts'
import { getPreferences } from '../../preferences.ts'
import { getCurrentGraph } from '../../routing.ts'
import { WsEventTypes } from '../../types.ts'

const router = new OakRouter()

const sockets = new Set<WebSocket>()

router.get('/ws', (ctx) => {
  const sock = ctx.upgrade({ protocol: 'brabbelbox' })
  sock.onopen = () => {
    sockets.add(sock)

    // send current state
    try {
      send(sock, 'prefs', getPreferences())
      send(sock, 'graph', getCurrentGraph())
      send(sock, 'mixer', getMixerMatrix())
    } catch (_err) {
      // we don't ensure that this data is transmitted
    }
  }
  sock.onerror = (e) => {
    console.log('socket errored:', e)
    sock.close()
  }
  sock.onclose = () => {
    sockets.delete(sock)
    console.log('socket closed')
  }
})

export function send(socket: WebSocket, event: WsEventTypes, body: any) {
  socket.send(JSON.stringify({ event, body }))
}

export function broadcast(event: WsEventTypes, body: any) {
  const data = JSON.stringify({
    event,
    body,
  })

  for (const socket of sockets.values()) {
    try {
      socket.send(data)
    } catch (err) {
      console.log(err)
    }
  }
}

export default router
