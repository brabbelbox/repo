import { OakRouter } from '../../deps.ts'
import { getPreferences, setPreferences } from '../../preferences.ts'
import { Preferences } from '../../types.ts'

import { validatePreferencesInput } from './inputValidation.ts'

const router = new OakRouter()

router.get('/preferences', (ctx) => {
  const prefs = getPreferences()
  ctx.response.body = prefs
})

router.patch('/preferences', async (ctx) => {
  const body: Preferences = await ctx.request.body({ type: 'json' }).value
  validatePreferencesInput(body, ctx)
  setPreferences(body)
  ctx.response.body = {}
})

export default router
