import { type Context, HTTPStatus } from '../../deps.ts'
import { Element, ElementType, LanguageTuple, Preferences } from '../../types.ts'

function langExists(code: string) {
  // TODO: proper validation
  return code.length === 2 || code.length === 3
}

function validateLangTuple(
  lang: Partial<LanguageTuple>,
  providesSomething: boolean,
  acceptsSomething: boolean,
  ctx: Context,
) {
  if (typeof lang.accepts !== 'undefined') {
    ctx.assert(
      langExists(lang.accepts),
      HTTPStatus.BadRequest,
      `unknown language '${lang.accepts}'`,
    )
    ctx.assert(
      acceptsSomething,
      HTTPStatus.BadRequest,
      "can't set an accepting language on an output-only element",
    )
  }
  if (typeof lang.provides !== 'undefined') {
    ctx.assert(
      langExists(lang.provides),
      HTTPStatus.BadRequest,
      `unknown language '${lang.provides}'`,
    )
    ctx.assert(
      providesSomething,
      HTTPStatus.BadRequest,
      "can't set an providing language on an input-only element",
    )
  }
  if (typeof lang.priority !== 'undefined') {
    ctx.assert(
      typeof lang.priority == 'number' && lang.priority > 0 && lang.priority < 2,
      HTTPStatus.BadRequest,
      `invalid language priority '${lang.priority}'`,
    )
  }
}

export function validatePatchBody(body: Partial<Element>, type: ElementType, ctx: Context) {
  const keysAllowedToGetUpdates = ['langs', 'isDisabled']
  const providesSomething = type === ElementType.Interpreter || type === ElementType.Stage
  const acceptsSomething = type === ElementType.Interpreter || type === ElementType.Transmitter
  if (providesSomething) {
    keysAllowedToGetUpdates.push('provideGain')
    keysAllowedToGetUpdates.push('isOutputMuted')
  }
  if (acceptsSomething) {
    keysAllowedToGetUpdates.push('acceptGain')
  }
  if (type === ElementType.Transmitter) {
    keysAllowedToGetUpdates.push('txChannel')
  }

  for (const key in body) {
    ctx.assert(
      keysAllowedToGetUpdates.includes(key),
      HTTPStatus.BadRequest,
      `it is not allowed to set '${key}' on element of type '${type}'`,
    )
  }
  if (body.langs) {
    ctx.assert(Array.isArray(body.langs), HTTPStatus.BadRequest, 'langs must be of type array')
    for (const lang of body.langs) {
      validateLangTuple(lang, providesSomething, acceptsSomething, ctx)
    }
  }
  if (typeof body.provideGain !== 'undefined') {
    ctx.assert(
      typeof body.provideGain == 'number' && body.provideGain >= 0 && body.provideGain <= 8,
      HTTPStatus.BadRequest,
      `invalid provideGain '${body.provideGain}'`,
    )
  }
  if (typeof body.acceptGain !== 'undefined') {
    ctx.assert(
      typeof body.acceptGain == 'number' && body.acceptGain >= 0 && body.acceptGain <= 8,
      HTTPStatus.BadRequest,
      `invalid acceptGain '${body.provideGain}'`,
    )
  }
  if (typeof body.txChannel !== 'undefined') {
    ctx.assert(
      (typeof body.txChannel == 'number' && body.txChannel >= -15 && body.txChannel <= 15) ||
        body.txChannel === null,
      HTTPStatus.BadRequest,
      `invalid txChannel '${body.txChannel}'`,
    )
  }
  if (typeof body.isOutputMuted !== 'undefined') {
    ctx.assert(
      typeof body.isOutputMuted == 'boolean',
      HTTPStatus.BadRequest,
      `invalid isOutputMuted type '${body.txChannel}'`,
    )
  }
  if (typeof body.isDisabled !== 'undefined') {
    ctx.assert(
      typeof body.isDisabled == 'boolean',
      HTTPStatus.BadRequest,
      `invalid isDisabled type '${body.isDisabled}'`,
    )
  }
}

export function validatePreferencesInput(_prefs: Preferences, _ctx: Context) {
  // TODO
  console.warn('TODO: validatePreferencesInput() is not defined yet')
}
