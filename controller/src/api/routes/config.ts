import { config } from '../../config.ts'
import { OakRouter } from '../../deps.ts'
const router = new OakRouter()

router.get('/config', (ctx) => {
  ctx.response.body = config
})

export default router
