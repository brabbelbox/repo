import { OakRouter } from '../../deps.ts'
import { Element } from '../../types.ts'
import { validatePatchBody } from './inputValidation.ts'
import { getPreferences, updateElement } from '../../preferences.ts'

const router = new OakRouter()
router.patch('/element/:id', async (ctx) => {
  const prefs = getPreferences()
  const body: Partial<Element> = await ctx.request.body({ type: 'json' }).value

  const element = prefs.elements.find((el) => el.id === ctx.params.id)
  if (!element) {
    return ctx.throw(404, 'Element does not exist')
  }
  validatePatchBody(body, element.type, ctx)

  updateElement(ctx.params.id, body)

  ctx.response.status = 200
})

export default router
