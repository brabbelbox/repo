import { OakRouter } from '../../deps.ts'
import preferences from './preferences.ts'
import config from './config.ts'
import element from './element.ts'
import ws from './ws.ts'

const router = new OakRouter()
router.use('/api', config.routes(), config.allowedMethods())
router.use('/api', element.routes(), element.allowedMethods())
router.use('/api', preferences.routes(), preferences.allowedMethods())
router.use('/api', ws.routes(), ws.allowedMethods())

export default router
