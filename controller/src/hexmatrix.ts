import { broadcast } from './api/routes/ws.ts'
import { config } from './config.ts'
import { WsEventTypes } from './types.ts'

type MixerMatrix = number[][]

function hexmatrixUrl(path: string) {
  if (config.hexmatrixUrl.endsWith('/') && path.startsWith('/')) path = path.slice(1)
  let url = config.hexmatrixUrl + path

  // relative url? add a host
  if (url.startsWith('/')) {
    url = 'http://localhost' + url
  }
  return url
}
function hexmatrixWsUrl(path: string) {
  return hexmatrixUrl(path).replace('http://', 'ws://')
}

function isMatrixSame(a: MixerMatrix, b: MixerMatrix) {
  if (a.length !== b.length) return false
  for (let i = 0; i < a.length; i++) {
    for (let j = 0; j < a[0].length; j++) {
      if (a[i][j] !== b[i][j]) return false
    }
  }
  return true
}

/**
 * send the matrix to our actual audio mixer
 */
export async function updateMixer(matrix: MixerMatrix) {
  try {
    await fetch(hexmatrixUrl('/matrix'), {
      method: 'post',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify(matrix),
    })
    console.log('[mixer] update applied')
  } catch (err) {
    console.error(err)
  }
}

async function fetchMixerMatrix(): Promise<MixerMatrix> {
  const res = await fetch(hexmatrixUrl('/matrix'))
  const data = await res.json()
  return data
}

let prevMatrix: MixerMatrix | null = null
let i = 0
async function pollMixerState() {
  try {
    const matrix = await fetchMixerMatrix()

    // on matrix has changed and also regularly (every 10s)
    // just to be sure the frontend is not missing something
    if (!prevMatrix || !isMatrixSame(matrix, prevMatrix) || (i++ % 10) === 0) {
      prevMatrix = matrix
      broadcast(WsEventTypes.Mixer, matrix)
    }
  } catch (err) {
    console.log(err)
  }
}

let mixerStatePollingInterval: number
export function startMixerPolling() {
  mixerStatePollingInterval = setInterval(pollMixerState, 1000)
}

export async function reloadMixerState() {
  // pause regular mixer state polling, to avoid race conditions
  clearInterval(mixerStatePollingInterval)

  // poll state now
  await pollMixerState()

  // restart regular polling
  startMixerPolling()
}

export function getMixerMatrix(): MixerMatrix {
  return prevMatrix || []
}

export async function getMixerHealth(): Promise<{ isRunning: boolean; connectedPorts: number }> {
  try {
    const res = await fetch(hexmatrixUrl('/health'))
    if (res.status == 200) {
      const body = await res.json()
      return { isRunning: true, connectedPorts: body.in_connections + body.out_connections }
    } else if (res.status >= 400 && res.status < 500) {
      // this doesn't seem like our hexmatrix
      return { isRunning: false, connectedPorts: 0 }
    } else {
      return { isRunning: true, connectedPorts: 0 }
    }
  } catch (_err) {
    return { isRunning: false, connectedPorts: 0 }
  }
}

let lastMeters: { in: number[]; out: number[] } = {
  in: [],
  out: [],
}

export function watchMixerMeters() {
  const websocket = new WebSocket(hexmatrixWsUrl('/ws'), 'meters')
  websocket.onmessage = (ev) => {
    try {
      const data = JSON.parse(ev.data)
      const meters = data.meters

      if (!lastMeters || JSON.stringify(lastMeters) !== JSON.stringify(meters)) {
        broadcast(WsEventTypes.Meters, meters)
        lastMeters = meters
      }
      // deno-lint-ignore no-empty
    } catch (_err) {}
  }
  websocket.onerror = (err) => {
    console.log(`Error while connecting to hexmatrix: ${err.message}`)
    websocket.close()
  }
  websocket.onclose = () => {
    setTimeout(watchMixerMeters, 1000)
  }
}
