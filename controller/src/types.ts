export interface LanguageTuple {
  // language the person/transmitter/... hears
  accepts?: string

  // language the person/stage/... speaks
  provides?: string

  // higher means more preferred
  priority?: number
}
export enum ElementType {
  Stage = 'stage',
  Interpreter = 'int',
  Transmitter = 'tx',
}
export interface Element {
  id: string
  type: ElementType
  langs: LanguageTuple[]
  acceptGain?: number
  provideGain?: number
  isOutputMuted?: boolean
  txChannel?: number | null
  isDisabled?: boolean
}

export type MixerChannelMapping = { [outputChannel: number]: { [inputChannel: number]: number } }
export interface Preferences {
  enabledLangs: string[]
  speakerLangs: string[]
  elements: Element[]
  txBaseFreq: number
  outputActive?: boolean
  mixerOverrides: MixerChannelMapping
}

export interface Config {
  mixerChannelMapping: {
    in: { [id: string]: number }
    out: { [id: string]: number }
  }
  languagesSpotlight: string[]
  languagePriorities: { [code: string]: number; default: number }
  interpreterLanguageTuplePriorities: { high: number; normal: number; low: number }
  maxInterpreter: number
  txBandwith: number
  greetings: string
  mixerAdd: ElementOutputInputMapping
  hexmatrixUrl: string
  gnuradioPipewireLatency: string
  radioUsb: string
  audioInterfaceUsb: string
}

export interface Status {
  mixerRunning: boolean
  mixerConnections: number
  audioInterfaceFound: boolean
  radioFound: boolean
  radioActive: boolean
  cpuUsage: number[]
  memFree: number
}

export type ElementVolumeMapping = { [input: string]: number }
export type ElementOutputInputMapping = { [output: string]: ElementVolumeMapping }

export interface ReducedElement {
  id: string
  accepts?: string
  provides?: string
}

export enum WsEventTypes {
  Graph = 'graph',
  Meters = 'meters',
  Prefs = 'prefs',
  Mixer = 'mixer',
  Status = 'status',
}
