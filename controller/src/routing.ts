import { findArrangement, ReducedElement } from '../../graph/mod.ts'
import { broadcast } from './api/routes/ws.ts'
import { config } from './config.ts'
import { updateMixer } from './hexmatrix.ts'
import { getLogger } from './logging.ts'
import { Element, ElementOutputInputMapping, ElementType, MixerChannelMapping } from './types.ts'

const log = getLogger('routing')

type MixerMatrix = number[][]
type Graph = ReducedElement[]

interface Parameter {
  elements: Element[]
  mixerOverrides: MixerChannelMapping
}

let currentGraph: Graph = []

/**
 * generates the graph and applies all additional modifications
 * (fallbacks, gains, mutes, overrides)
 */
export async function apply({ elements, mixerOverrides }: Parameter) {
  log.debug('apply graph and modifications')
  const graph = generateGraph(elements)

  addElementFallbacks(graph, elements)

  currentGraph = graph
  broadcast('graph', currentGraph)

  const elementMapping = graphToBasicOutputInputMapping(graph)

  // TODO: fading - mix in previous inputs

  addElementSiblings(elementMapping, graph)
  applyElementGains(elementMapping, elements)
  applyElementMutes(elementMapping, elements)

  const mixerMatrix = mappingToMixerMatrix(elementMapping)
  applyMixerOverrides(mixerMatrix, mixerOverrides)

  await updateMixer(mixerMatrix)
}

export function getCurrentGraph(): Graph {
  return currentGraph
}

/**
 * uses the graph algorithm to find the best interpreter arrangement
 */
function generateGraph(elements: Element[]): ReducedElement[] {
  try {
    return findArrangement(elements, config.languagePriorities)
  } catch (err) {
    log.error(err.message)
    // even when not finding any graph, the stage is still providing something
    const stage = elements.find((el) => el.type === ElementType.Stage)
    return [
      {
        id: 'stage',
        provides: stage?.langs[0].provides,
      },
    ]
  }
}

/**
 * inpreter which are "unused" should still be able to listen to the ongoing event
 * for that an accepted language with the lowest distance is mixed in as a fallback
 */
function addElementFallbacks(graph: Graph, elements: Element[]): void {
  const providedLangs = graph.map((e) => e.provides).filter((e) => e)
  const stageLang = graph.find((e) => e.id === 'stage')?.provides || ''
  for (const el of elements) {
    if (el.type !== ElementType.Interpreter) {
      // only apply on interpreter
      continue
    }

    let graphEl = graph.find((e) => e.id === el.id)
    if (!graphEl) {
      graphEl = {
        id: el.id,
      }
      graph.push(graphEl)
    }
    if (graphEl?.accepts) {
      // interpreter is already listening to something
      continue
    }

    const acceptedAndProvidedLangs = el.langs.map((l) => l.accepts).filter((l) =>
      providedLangs.includes(l)
    )
    // prefer the stageLang if accepted - or if there is no other option
    if (acceptedAndProvidedLangs.includes(stageLang) || !acceptedAndProvidedLangs.length) {
      graphEl.accepts = stageLang
    } else {
      graphEl.accepts = acceptedAndProvidedLangs[0]
      // TODO: pick the language with the lowest distance
    }
  }
}

/**
 * transforms the graph into a mapping in the form
 *    mapping[output][input] = volume
 */
function graphToBasicOutputInputMapping(graph: Graph) {
  const providers: { [lang: string]: Set<string> } = {}
  for (const el of graph) {
    if (!el.provides) continue
    if (!providers[el.provides]) providers[el.provides] = new Set()
    providers[el.provides].add(el.id)
  }

  // map input to outputs
  const outputMapping: ElementOutputInputMapping = {}
  for (const el of graph) {
    if (el.accepts) {
      if (!providers[el.accepts]) {
        log.warning(`${el.id} expects ${el.accepts}, but the language is not provided anywhere`)
        continue
      }
      outputMapping[el.id] = {}
      providers[el.accepts].forEach((input) => {
        outputMapping[el.id][input] = 1.0
      })
    }
  }
  return outputMapping
}

/**
 * each element can have acceptGain and/or provideGain
 * these are added to the volumes in the mapping
 */
function applyElementGains(mapping: ElementOutputInputMapping, elements: Element[]): void {
  for (const output in mapping) {
    const acceptGain = elements.find((e) => e.id === output)?.acceptGain ?? 1

    // apply output gain to all its inputs
    for (const input in mapping[output]) {
      mapping[output][input] *= acceptGain

      // also apply input gain
      const provideGain = elements.find((e) => e.id === input)?.provideGain ?? 1
      mapping[output][input] *= provideGain
    }
  }
}

/**
 * interpreter could have their output (=microphone) muted
 * this is applied here via setting the gain to zero
 */
function applyElementMutes(mapping: ElementOutputInputMapping, elements: Element[]): void {
  // apply mutes
  for (const output in mapping) {
    for (const input in mapping[output]) {
      const isInputMuted = elements.find((e) => e.id === input)?.isOutputMuted
      if (isInputMuted) mapping[output][input] = 0
    }
  }
}

/**
 * when multiple interpreter speak the same language in parallell, they should be able to hear each other
 */
function addElementSiblings(mapping: ElementOutputInputMapping, graph: Graph): void {
  for (const el of graph) {
    if (!el.provides) continue
    const otherProvider = graph.filter((e) => e.provides == el.provides && e.id !== el.id)

    for (const provider of otherProvider) {
      mapping[el.id][provider.id] = 0.2
    }
  }
}

const MATRIX_IN_SIZE = 16
const MATRIX_OUT_SIZE = 32
/**
 * Transforms the input->output mapping with ID's to the two dimensional matrix which our
 * hexmatrix fork can understand
 */
function mappingToMixerMatrix(mapping: ElementOutputInputMapping): MixerMatrix {
  // generate matrix with zeros
  const matrix: number[][] = []
  for (let i = 0; i < MATRIX_OUT_SIZE; i++) {
    matrix[i] = []
    for (let j = 0; j < MATRIX_IN_SIZE; j++) matrix[i][j] = 0
  }
  for (const output in mapping) {
    const outputChannel = config.mixerChannelMapping.out[output]
    if (!outputChannel) {
      throw new Error(`a channel for output '${output}' is not defined in mixerChannelMapping`)
    }
    for (const input in mapping[output]) {
      const inputChannel = config.mixerChannelMapping.in[input]
      if (!inputChannel) {
        throw new Error(`a channel for input '${input}' is not defined in mixerChannelMapping`)
      }
      matrix[outputChannel - 1][inputChannel - 1] = mapping[output][input]
    }
  }
  return matrix
}

/**
 * overrides any value in the mixer matrix
 */
function applyMixerOverrides(matrix: MixerMatrix, overrides: MixerChannelMapping) {
  for (const outputChannel in overrides) {
    for (const inputChannel in overrides[outputChannel]) {
      matrix[parseInt(outputChannel) - 1][parseInt(inputChannel) - 1] =
        overrides[outputChannel][inputChannel]
    }
  }
}
