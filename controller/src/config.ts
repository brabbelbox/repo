import { path, yamlParse } from './deps.ts'
import { getLogger } from './logging.ts'
import { Config } from './types.ts'

const log = getLogger('config')

const configFile = path.join(path.dirname(path.fromFileUrl(import.meta.url)), '../config.yaml')

log.debug('reading config.yaml')
export const config: Config = yamlParse(await Deno.readTextFile(configFile)) as any
