import { bold, italic, log } from './deps.ts'

const loggers: { [name: string]: log.LoggerConfig } = {
  default: {
    level: Deno.env.get('BRABBEL_LOG_LEVEL') as log.LevelName || 'DEBUG',
    handlers: ['console'],
  },
}

export function getLogger(moduleName: string) {
  // create a new logger for this module
  loggers[moduleName] = loggers.default

  // recall setup function
  log.setup({
    handlers: {
      console: new log.handlers.ConsoleHandler('DEBUG', {
        formatter: (logRecord) => {
          let msg = `[${logRecord.datetime.toISOString()}] ${logRecord.levelName}/${
            bold(logRecord.loggerName)
          }: ${logRecord.msg}`

          logRecord.args.forEach((arg) => {
            msg += ' ' + italic(Deno.inspect(arg))
          })

          return msg
        },
      }),
    },

    loggers,
  })
  return log.getLogger(moduleName)
}

const oakLog = getLogger('oak')

// deno-lint-ignore ban-types no-explicit-any
export async function oakLogger({ response, request }: any, next: Function) {
  await next()

  const User = request.headers.get('User-Agent')
  const status: number = response.status
  const logString =
    `${request.ip} "${request.method} ${request.url.pathname}" ${response.status} ${User}`

  if (status >= 500) {
    oakLog.error(logString)
  } else if (status >= 400) {
    oakLog.warning(logString)
  } else {
    oakLog.info(logString)
  }
}
