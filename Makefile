
VITE_API_URL ?= /controller
HOST ?= brabbelbox
REMOTE ?= ubuntu@${HOST}

run-controller:
	cd controller && \
	deno run --allow-net --allow-read --allow-write --allow-run src/server.ts 1111

build-frontend:
	cd frontend && \
	yarn && \
	VITE_API_URL=$(VITE_API_URL) yarn build

deploy-controller:
	cd controller && rm -rf vendor && deno vendor --output vendor --no-config src/server.ts
	cd graph && deno bundle mod.ts bundle.ts
	rsync -avzz --delete \
		--exclude config.yaml \
		./controller/src ./controller/vendor \
		${REMOTE}:/opt/brabbelbox/controller/
	rsync -avzz --mkpath \
		./graph/bundle.ts \
		${REMOTE}:/opt/brabbelbox/graph/mod.ts
	ssh -t ${REMOTE} systemctl --user restart brabbelbox-controller.service

deploy-frontend: build-frontend
	rsync -avzz --delete \
		--exclude config.yaml \
		./frontend/dist/* \
		${REMOTE}:/opt/brabbelbox/frontend

deploy-mixer:
	cd mixer && go build .
	rsync -av mixer/hexmatrix-go ${REMOTE}:/opt/brabbelbox/bin/
	ssh -t ${REMOTE} systemctl --user restart brabbelbox-mixer.service

deploy-plumber:
	scp pw-connections.yml ${REMOTE}:/opt/brabbelbox/pw-connections.yml
	ssh -t ${REMOTE} systemctl --user restart brabbelbox-plumber.service

deploy-gnuradio:
	rsync -avzz --delete \
		--exclude target \
		./gnuradio/* \
		${REMOTE}:/opt/brabbelbox/gnuradio

deploy: deploy-gnuradio deploy-mixer deploy-plumber deploy-frontend deploy-controller

ansible-setup:
	cd ansible && ansible-playbook --extra-vars ansible_host="${HOST}" setup.playbook.yml

restart:
	ssh -t ${REMOTE} sudo reboot || echo ""

# everything together
provision: ansible-setup deploy restart
