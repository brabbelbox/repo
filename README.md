# Brabbelbox v1 main repo

## Development

Start the services in the described order.

### mixer

```bash
cd mixer
pw-jack go run .
```

### controller

We're using [deno] and [denon](https://github.com/denosaurs/denon) for easy development.

```bash
cd controller
denon start
```

### frontend

Since deno's npm compatibility is not perfect yet, we rely on [node](https://nodejs.org), yarn & vite for the frontend development.

```bash
cd frontend
yarn
yarn dev
```
