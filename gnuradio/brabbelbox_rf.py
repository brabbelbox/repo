from gnuradio import analog
from gnuradio import audio
from gnuradio import filter
from gnuradio import blocks
from gnuradio.filter import firdes
from gnuradio import gr
from gnuradio.fft import window
import sys
import signal
from argparse import ArgumentParser, RawTextHelpFormatter
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import osmosdr
import time


parser = ArgumentParser(
                    prog = 'Brabbelbox RF',
                    description = 'This registers as a pipewire device and forwards the audio to an OsmoSDR Sink (e.g. HackRF) as multiple FM channels combined.',
                    epilog = "The frequencies of the individual FM channels are calculated as follows:\n    center_frequency + channel * 0.2 MHz",
                    formatter_class=RawTextHelpFormatter,
                    # allow negative channel numbers as args
                    # this is apparently the only way to make it possible with argpase
                    # https://github.com/python/cpython/issues/53580
                    prefix_chars="/")
parser.add_argument('center_frequency', help="frequency in MHz where channel 0 lies")
parser.add_argument('channels', help="comma separated list of channels -15...15 which are used for the FM frequencies")
parser.add_argument('/enable-gain', action='store_const', const=True, help="raises the HackRF gain to +15dB")

args = parser.parse_args()

class polyphase(gr.top_block):

    def __init__(self, base_freq, channel_map, enable_gain=False):
        gr.top_block.__init__(self, "multi_fm_transmit")

        ##################################################
        # Variables
        ##################################################
        self.num_banks = num_banks = 30
        self.mod_rate = mod_rate = 200000
        self.taps_rs = taps_rs = firdes.low_pass_2(25.0, 25.0, 0.1, 0.1, 60, window.WIN_KAISER, 7.0)
        self.taps = taps = firdes.low_pass_2(8, num_banks*mod_rate, 120e3, 50e3, 80, window.WIN_BLACKMAN_HARRIS)
        self.channel_map = channel_map
        self.channel_count = len(channel_map)
        self.base_freq = base_freq
        self.aud_rate = aud_rate = 48000

        # same as channel_map, but without disabled channels
        # Background: allowing to specify audio channels which are not transmitted makes
        # the channel mapping in the controller and automated pipwire connections easier
        active_channel_map = [ch for ch in channel_map if ch != None]
        active_channel_count = len(active_channel_map)

        # correct negative channel indexes
        # the polyphase synthesizer works only works with positive integers,
        # n > num_banks/2 being mapped below the center_freq
        for i in range(active_channel_count):
          if active_channel_map[i] < 0:
            active_channel_map[i] += num_banks

        ##################################################
        # Blocks
        ##################################################

        # audio source
        self.audio_source = audio.source(aud_rate, 'polyphase-fm', False)

        # polyphase synthesizer
        self.pfb_synthesizer_ccf = filter.pfb_synthesizer_ccf(
            num_banks,
            taps,
            False)
        self.pfb_synthesizer_ccf.set_channel_map(active_channel_map)
        self.pfb_synthesizer_ccf.declare_sample_delay(0)

        # osmosdr
        self.osmosdr_sink = osmosdr.sink(
            args="numchan=" + str(1) + " " + "hackrf,buffers=1"
        )
        self.osmosdr_sink.set_sample_rate(mod_rate*num_banks)
        self.osmosdr_sink.set_center_freq(base_freq, 0)
        self.osmosdr_sink.set_freq_corr(0, 0)
        gain = 15 if enable_gain else 0
        self.osmosdr_sink.set_gain(gain, 0)
        self.osmosdr_sink.set_if_gain(0, 0)
        self.osmosdr_sink.set_bb_gain(0, 0)
        self.osmosdr_sink.set_antenna('', 0)
        self.osmosdr_sink.set_bandwidth(0, 0)

        # per channel blocks
        self.rational_resampler = []
        self.analog_wfm_tx = []
        for i in range(active_channel_count):
            self.rational_resampler.insert(i, filter.rational_resampler_fff(
                interpolation=25,
                decimation=6,
                taps=taps_rs,
                fractional_bw=0.3
            ))
            self.analog_wfm_tx.insert(i, analog.wfm_tx(
                audio_rate=mod_rate,
                quad_rate=mod_rate,
                tau=75e-6,
                max_dev=75e3,
                fh=10e3,
            ))

        ##################################################
        # Connections
        ##################################################
        self.connect((self.pfb_synthesizer_ccf, 0), (self.osmosdr_sink, 0))

        gnuradio_channel = 0
        for i, ch in enumerate(channel_map):
            if ch != None:
              self.connect((self.audio_source, i), (self.rational_resampler[gnuradio_channel], 0))
              self.connect((self.rational_resampler[gnuradio_channel], 0), (self.analog_wfm_tx[gnuradio_channel], 0))
              self.connect((self.analog_wfm_tx[gnuradio_channel], 0), (self.pfb_synthesizer_ccf, gnuradio_channel))
              gnuradio_channel += 1
            else:
              # no frequency/txChannel specified for this audio channel
              # let's pipe it into a null sink
              self.connect((self.audio_source, i), (blocks.null_sink(gr.sizeof_float), 0))

def to_int_or_none(value):
  try:
      return int(value)
  except:
      return None

def main():
    print("\n")
    if not gr.version().startswith('3.10'):
        print("Error: this script only works with GNU Radio 3.10, but you're using "+gr.version())
        sys.exit(1)

    if gr.prefs().get_string("audio", "audio_module", "auto") != "jack":
        print("Error: environment variable GR_CONF_AUDIO_AUDIO_MODULE=jack is required")
        sys.exit(1)

    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print("Error: failed to enable real-time scheduling.")
        sys.exit(1)


    center_freq = int(int(float(args.center_frequency)*10)/10 * 1e6)
    channel_map = list(map(lambda c: to_int_or_none(c), args.channels.split(',')))

    # raises the HackRF gain to +15dB
    enable_gain = args.enable_gain

    print("Starting Brabbelbox RF with following channels:")

    for i, ch in enumerate(channel_map):
        if ch != None:
          freq = (center_freq + ch*0.2e6)/1e6
          print("- Audio Input "+str(i+1)+" -> "+str(round(freq, 1))+" MHz")
        else:
          print("- Audio Input "+str(i+1)+" -> disabled")
    print("\n")

    tb = polyphase(center_freq, channel_map, enable_gain)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start(200000)
    tb.wait()


if __name__ == '__main__':
    main()
