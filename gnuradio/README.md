# SDR Part for the brabbelbox

kindly developed by [NerdyProjects](https://github.com/NerdyProjects)

look at the original repo for more details: https://github.com/NerdyProjects/brabbelbox_rf

## Requirements
- gnuradio 3.10
- gr-osmosdr
- pipewire-jack

## Usage
```bash
GR_CONF_AUDIO_AUDIO_MODULE=jack pw-jack python brabbelbox_rf.py  [/h] [/enable-gain] base_frequency channels
```
```
This registers as a pipewire device and forwards the audio to an OsmoSDR Sink (e.g. HackRF) as multiple FM channels combined.

positional arguments:
  center_frequency  frequency in MHz where channel 0 lies
  channels          comma separated list of channels -15...15 which are used for the FM frequencies


optional arguments:
  /h, //help      show this help message and exit
  /enable-gain   raises the HackRF gain to +15dB

The frequencies of the individual FM channels are calculated as follows:
    center_frequency + channel * 0.2 MHz
```
