import { createI18n } from 'vue-i18n'
import en from './locales/en'

type MessageSchema = typeof en

export default createI18n<[MessageSchema], 'en'>({
  locale: 'en',
  fallbackLocale: 'en',
  legacy: false,
  messages: {
    en,
  },
})
