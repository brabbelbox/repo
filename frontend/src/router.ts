import { createRouter, createWebHistory } from 'vue-router'
import Welcome from './pages/Welcome.vue'
import Mixer from './pages/Mixer.vue'
import Settings from './pages/Settings.vue'
import Debug from './pages/Debug.vue'

const routes = [
  { path: '/', redirect: '/welcome' },
  { path: '/welcome', component: Welcome },
  { path: '/settings', component: Settings },
  { path: '/mixer', component: Mixer },
  { path: '/debug', component: Debug },
]

export default createRouter({
  history: createWebHistory(),
  routes,
})
