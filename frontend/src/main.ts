import { createApp } from 'vue'
import '@picocss/pico'
import { library as iconLibrary } from '@fortawesome/fontawesome-svg-core'
import {
  faBug,
  faCirclePlus,
  faTimesCircle,
  faGear,
  faSliders,
  faCouch,
  faHeadset,
  faHeadphones,
  faMicrophone,
  faMicrophoneSlash,
  faTowerBroadcast,
  faTriangleExclamation,
  faPencil,
  faTrash,
  faBars,
  faFloppyDisk,
  faCircleExclamation,
  faQuestion,
} from '@fortawesome/free-solid-svg-icons'
import './style.css'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import i18n from './i18n'

const pinia = createPinia()

iconLibrary.add(
  faBug,
  faCirclePlus,
  faGear,
  faSliders,
  faTimesCircle,
  faCouch,
  faHeadset,
  faHeadphones,
  faMicrophone,
  faMicrophoneSlash,
  faTowerBroadcast,
  faTriangleExclamation,
  faPencil,
  faTrash,
  faBars,
  faFloppyDisk,
  faCircleExclamation,
  faQuestion,
)

const app = createApp(App)
app.use(pinia)
app.use(router)
app.use(i18n)
app.mount('#app')
