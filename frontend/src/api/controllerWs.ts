/* eslint-disable import/prefer-default-export */
import { WsEventTypes } from '@controller/src/types'
import WebSocketConnection from './webSocketConnection'
import { apiWsUrl } from './common'

let wsc: WebSocketConnection
type EventTypes = WsEventTypes | 'connected' | 'disconnected'
const handlers = new Map<EventTypes, Set<Function>>()

export function register(event: EventTypes, handler: Function) {
  if (handlers.get(event)) {
    handlers.get(event)?.add(handler)
  } else {
    handlers.set(event, new Set([handler]))
  }
}

function dispatch(event: EventTypes, body: any = {}) {
  const callbacks = handlers.get(event)
  if (!callbacks) return
  callbacks.forEach((callback) => {
    callback(body)
  })
}

export function connect() {
  return new Promise((resolve) => {
    wsc = new WebSocketConnection(
      apiWsUrl('/ws'),
      'brabbelbox',
      (event: MessageEvent) => {
        const data = JSON.parse(event.data)
        dispatch(data.event, data.body)
      },
      (state: EventTypes) => {
        dispatch(state)
      },
    )
    wsc.connect()
    let gotInitialData = false
    register(WsEventTypes.Prefs, () => {
      if (!gotInitialData) {
        gotInitialData = true
        resolve({})
      }
    })
  })
}

export function disconnect() {
  wsc.disconnect()
}
