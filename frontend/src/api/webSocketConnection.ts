export default class {
  protected socket?: WebSocket

  protected endpoint: string | URL

  protected protocols: string | string[]

  protected handler: (event: MessageEvent) => void

  protected onStateChange: (state: 'connected' | 'disconnected') => void

  protected reconnectAfter: number | false

  constructor(
    endpoint: string | URL,
    protocols: string | string[],
    handler: (event: MessageEvent) => void,
    onStateChange: (state: 'connected' | 'disconnected') => void = () => {},
    reconnectAfter: number | false = 1000,
  ) {
    this.endpoint = endpoint
    this.protocols = protocols
    this.handler = handler
    this.onStateChange = onStateChange
    this.reconnectAfter = reconnectAfter
  }

  connect() {
    this.socket = new WebSocket(this.endpoint, this.protocols)
    this.socket.addEventListener('open', () => {
      this.onStateChange('connected')
    })
    this.socket.addEventListener('message', this.handler)
    this.socket.addEventListener('error', this.handleError.bind(this), { once: true })
  }

  disconnect() {
    if (!this.socket) return

    this.onStateChange('disconnected')
    this.socket.close()
    this.socket.removeEventListener('error', this.handleError.bind(this))
  }

  private handleError(event: Event) {
    console.warn('WebSocket connection to mixer encountered error: ', event, 'Closing socket')
    this.onStateChange('disconnected')
    this.socket?.close()

    if (this.reconnectAfter) {
      console.log(`Retrying connection in ${this.reconnectAfter / 1000}sec..`)
      setTimeout(() => {
        this.connect()
      }, this.reconnectAfter as number)
    }
  }
}
