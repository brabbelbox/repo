function urlHttpToWs(httpUrl: string) {
  const url = new URL(httpUrl, window.location.href)
  url.protocol = url.protocol.replace('http', 'ws')
  return url.href
}

const API_URL = import.meta.env.VITE_API_URL || `http://${document.location.hostname}:1111/api`

const API_WS_URL = urlHttpToWs(API_URL)

export function apiUrl(path: string) {
  return API_URL + path
}

export function apiWsUrl(path: string) {
  return API_WS_URL + path
}
