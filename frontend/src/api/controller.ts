import axios from 'axios'
import {
  Config, Element, LanguageTuple, Preferences,
} from '../../../controller/src/types'
import { apiUrl } from './common'

export async function getConfig(): Promise<Config> {
  const res = await axios.get(apiUrl('/config'))
  return res.data
}

export async function updatePreferences(prefs: Partial<Preferences>): Promise<void> {
  await axios.patch(apiUrl('/preferences'), prefs)
}

export interface ElementOptions extends Partial<Element> {
  langs?: LanguageTuple[]
  provideGain?: number
  acceptGain?: number
  isOutputMuted?: boolean
  txChannel?: number
  isDisabled?: boolean
}
export async function updateElement(id: string, data: ElementOptions): Promise<void> {
  await axios.patch(apiUrl(`/element/${id}`), data)
}
