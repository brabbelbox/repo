import { getLanguageName } from '@src/utils/language'
import { computed, unref } from 'vue'
import type { Ref } from 'vue'
import { useI18n } from 'vue-i18n'

export function useLanguageFromCode(code: string | Ref<string>) {
  return computed(() => {
    const { locale } = useI18n()
    return getLanguageName(unref(code), locale.value)
  })
}

export function useLanguagesFromCodes(codes: string[] | Ref<string[]>) {
  return computed(() => (
    unref(codes).map((code) => ({
      code,
      name: useLanguageFromCode(code).value,
    }))
  ))
}
