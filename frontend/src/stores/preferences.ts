import { defineStore } from 'pinia'
import {
  Preferences,
  Element,
  ElementType,
  ReducedElement,
  WsEventTypes,
} from '@controller/src/types'
import {
  ElementOptions, updateElement, updatePreferences,
} from '../api/controller'
import { register } from '../api/controllerWs'
import useConfigStore from './config'

export default defineStore('prefs', {
  state: (): Preferences & { currentGraph: ReducedElement[] } => ({
    enabledLangs: [],
    speakerLangs: [],
    elements: [],
    txBaseFreq: 0,
    outputActive: false,
    mixerOverrides: {},
    currentGraph: [],
  }),
  getters: {
    stageLanguage(state): string | null {
      const stage = state.elements.find((el) => el.id === 'stage')
      return stage?.langs?.[0].provides || null
    },
    requiredLanguages(state): string[] {
      const txs = state.elements.filter((el) => el.type === ElementType.Transmitter)
      return txs.map((el) => el.langs[0].accepts as string)
    },
    providedLanguages(state): string[] {
      if (!this.stageLanguage) return []
      const provided: string[] = [this.stageLanguage]
      // eslint-disable-next-line no-restricted-syntax
      for (const el of state.currentGraph) {
        if (el.provides && !provided.includes(el.provides)) {
          provided.push(el.provides)
        }
      }
      return provided
    },
    missingLanguages(): string[] {
      return this.requiredLanguages.filter((lang) => !this.providedLanguages.includes(lang))
    },
  },
  actions: {
    start() {
      register(WsEventTypes.Graph, (graph: ReducedElement[]) => {
        this.currentGraph = graph
      })
      register(WsEventTypes.Prefs, (prefs: Preferences) => {
        this.$patch(prefs)
      })
    },
    async setPreferences(preferences: Partial<Preferences>) {
      await updatePreferences(preferences)
    },
    async setInterpreters(interpreters: Element[]) {
      const others = this.elements.filter((el) => el.type !== ElementType.Interpreter)

      const elements = [
        ...others,
        ...interpreters,
      ]
      await updatePreferences({ elements })
      this.elements = elements
    },
    async setTransmitters(transmitters: Element[]) {
      const others = this.elements.filter((el) => el.type !== ElementType.Transmitter)

      const elements = [
        ...others,
        ...transmitters,
      ]
      await updatePreferences({ elements })
      this.elements = elements
    },
    async addElement(element: Element) {
      await updatePreferences({
        elements: [
          ...this.elements,
          element,
        ],
      })
    },
    async deleteElement(id: string) {
      const newElements = this.elements.filter((e) => e.id !== id)

      await updatePreferences({
        elements: newElements,
      })
    },
    async updateElement(id: string, data: ElementOptions) {
      await updateElement(id, data)
    },
    async setStageLanguage(lang: string) {
      const stage = this.$state.elements.find((el) => el.id === 'stage')
      if (!stage) return
      const prevLang = stage.langs[0].provides
      stage.langs[0].provides = lang
      try {
        await updateElement('stage', {
          langs: [{ provides: lang }],
        })
      } catch (err) {
        // revert change
        stage.langs[0].provides = prevLang
        console.error(err)
      }
    },
    async setElementAcceptGain(id: string, gain: number) {
      await updateElement(id, { acceptGain: gain })
    },
    async setElementProvideGain(id: string, gain: number) {
      await updateElement(id, { provideGain: gain })
    },
    async setElementOutputMute(id: string, state: boolean) {
      await updateElement(id, { isOutputMuted: state })
    },
    async setElementDisabled(id: string, state: boolean) {
      await updateElement(id, { isDisabled: state })
    },
    async setBaseFreq(freq: number) {
      await updatePreferences({
        txBaseFreq: freq,
      })
    },
    async setOutputActive(state: boolean) {
      await updatePreferences({
        outputActive: state,
      })
    },
    async setInputChannelPFL(inputChannel: number, active: boolean) {
      const monitorChannel = useConfigStore().getOutputChannelById('monitor')
      const currentlyMonitored = { ...this.mixerOverrides[monitorChannel] }
      if (active) {
        currentlyMonitored[inputChannel] = 1
      } else {
        delete currentlyMonitored[inputChannel]
      }
      const mixerOverrides = {
        ...this.mixerOverrides,
        [monitorChannel]: currentlyMonitored,
      }
      await updatePreferences({ mixerOverrides })
      this.mixerOverrides = mixerOverrides
    },
  },
})
