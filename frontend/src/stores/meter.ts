import { defineStore } from 'pinia'
import { WsEventTypes } from '@controller/src/types'
import { register } from '../api/controllerWs'

interface MeterStoreState {
  inputVolumes: number[]
  outputVolumes: number[]
}

export default defineStore('meter', {
  state: () => ({
    inputVolumes: [],
    outputVolumes: [],
  } as MeterStoreState),
  getters: {
    getInputVolume(state) {
      return (channel: number): number => state.inputVolumes[channel - 1] || -1
    },
    getOutputVolume(state) {
      return (channel: number): number => state.outputVolumes[channel - 1] || -1
    },
  },
  actions: {
    start() {
      register(WsEventTypes.Meters, (volumes: { in: number[], out: number[] }) => {
        this.inputVolumes = volumes.in
        this.outputVolumes = volumes.out
      })
    },
  },
})
