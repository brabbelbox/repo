import { defineStore } from 'pinia'
import { Status, WsEventTypes } from '@controller/src/types'
import usePrefsStore from './preferences'
import { register } from '../api/controllerWs'

export default defineStore('status', {
  state: (): Status & { controllerConnected: boolean } => ({
    controllerConnected: false,
    mixerRunning: false,
    mixerConnections: 0,
    audioInterfaceFound: false,
    radioFound: false,
    radioActive: false,
    cpuUsage: [],
    memFree: 0,
  }),
  getters: {
    errors(state) {
      const prefs = usePrefsStore()
      if (!state.controllerConnected) return ['controllerNotRunning']
      const errors: string[] = []
      if (!state.mixerRunning) errors.push('mixerNotRunning')
      else if (state.mixerConnections < 4) errors.push('mixerConnectionsMissing')
      if (!state.audioInterfaceFound) errors.push('audioInterfaceMissing')
      if (!state.radioFound) errors.push('radioMissing')
      if (state.radioFound && prefs.outputActive && !state.radioActive) errors.push('radioNotRunning')
      if (state.memFree < 50) errors.push('memoryFull')
      return errors
    },
    warnings(state) {
      const prefs = usePrefsStore()
      if (!state.controllerConnected) return []
      const warnings: string[] = []
      const cpuHigh = state.cpuUsage.filter((c) => c > 90)
      if (cpuHigh.length > state.cpuUsage.length / 2) warnings.push('highCpuUtil')
      if (state.memFree < 500 && state.memFree >= 50) warnings.push('memoryLow')
      if (prefs.missingLanguages.length) warnings.push('langMissing')
      return warnings
    },
  },
  actions: {
    start() {
      // listen to 'status' events via the controller websocket
      register(WsEventTypes.Status, (body: Status) => {
        this.$patch(body)
      })
      register('connected', () => {
        console.log('controller connected!')
        this.controllerConnected = true
      })
      register('disconnected', () => {
        this.controllerConnected = false
      })
    },
  },
})
