import { defineStore } from 'pinia'
import { WsEventTypes } from '@controller/src/types'
import { register } from '../api/controllerWs'
import useConfigStore from './config'

type Connection = { input: number, output: number, gain: number }

export default defineStore('mixer', {
  state: () => ({
    matrix: [] as number[][],
  }),
  getters: {
    connections(state): Connection[] {
      const connections: Connection[] = []
      for (let i = 0; i < state.matrix.length; i += 1) {
        const output = i + 1
        for (let j = 0; j < state.matrix[i].length; j += 1) {
          const input = j + 1
          const gain = state.matrix[i][j]
          if (gain > 0) {
            connections.push({ input, output, gain })
          }
        }
      }
      return connections
    },
    getConnectionsForOutputChannel() {
      return (outputChannel: number): Connection[] => this.connections
        .filter((c) => c.gain > 0)
        .filter((c) => c.output === outputChannel)
    },
    inputChannelCount(state) {
      return Math.max(...state.matrix.map((o) => o.length))
    },
    outputChannelCount(state) {
      return state.matrix.length
    },
    activePflChannels(): number[] {
      const configStore = useConfigStore()
      const pflOutput = configStore.getOutputChannelById('monitor')
      if (!pflOutput) return []
      const connections = this.getConnectionsForOutputChannel(pflOutput)
      return connections.map((c) => c.input)
    },
  },
  actions: {
    start() {
      register(WsEventTypes.Mixer, (matrix: number[][]) => {
        this.matrix = matrix
      })
    },
  },
})
