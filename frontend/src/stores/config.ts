import { defineStore } from 'pinia'
import { Config } from '../../../controller/src/types'
import { getConfig } from '../api/controller'

type ConfigStoreState = Pick<Config,
'greetings' |
'maxInterpreter' |
'languagesSpotlight' |
'languagePriorities' |
'interpreterLanguageTuplePriorities' |
'mixerChannelMapping' |
'txBandwith'
>
export default defineStore('config', {
  state: (): ConfigStoreState => ({
    greetings: '',
    maxInterpreter: 5,
    languagesSpotlight: [],
    languagePriorities: { default: 1 },
    interpreterLanguageTuplePriorities: { high: 1.1, normal: 1.0, low: 0.8 },
    mixerChannelMapping: { in: {}, out: {} },
    txBandwith: 0,
  }),
  getters: {
    getIdForChannel(state) {
      return (type: 'in' | 'out', num: number) => {
        const channels = state.mixerChannelMapping[type]
        // eslint-disable-next-line no-restricted-syntax
        for (const id in channels) {
          if (channels[id] === num) {
            return id
          }
        }
        return '-'
      }
    },
    getInputChannelById() {
      return (id: string) => this.mixerChannelMapping.in[id]
    },
    getOutputChannelById() {
      return (id: string) => this.mixerChannelMapping.out[id]
    },
  },
  actions: {
    async fetchConfig() {
      this.$patch(await getConfig())
    },
  },
})
