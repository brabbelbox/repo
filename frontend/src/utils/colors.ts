/* eslint-disable import/prefer-default-export */
export function rgbTransparency(rgb: string, opacity: number) {
  const r = parseInt(rgb.slice(1, 3), 16)
  const g = parseInt(rgb.slice(3, 5), 16)
  const b = parseInt(rgb.slice(5, 7), 16)

  return `rgba(${r}, ${g}, ${b}, ${opacity})`
}
