export function calculateFrequency(baseFreq: number, channel: number) {
  return (baseFreq + channel * 0.2).toFixed(1)
}

/**
 * channels have a 0.2 MHz distance to each other,
 * and channel 0 being on the center of the bandwith
 * a 5 MHz bandwith would therefore enable channels -12 to 12
 */
export function calculateMaxChannelIndex(txBandwith: number) {
  return Math.floor((txBandwith / 2) / 0.2)
}
