import { sha1 } from 'js-sha1'
import langCodes from './lang-codes.json'

function hashStringToColor(str: string) {
  return `#${sha1.hex(str).slice(0, 8)}`
}

const predefinedColors: { [code: string]: string } = {
  de: '#fbb200',
  es: '#ff0000',
  en: '#00aed6',
  fr: '#0000aa',
  ar: '#006C35',
}

export function getLanguageColor(code: string): string {
  if (!code) return '#aaaaaa'
  if (predefinedColors[code]) return predefinedColors[code]
  return hashStringToColor(code)
}

const languageNames: { [locale: string]: Intl.DisplayNames } = {}

export function getLanguageName(code: string, locale: string): string {
  if (!languageNames[locale]) languageNames[locale] = new Intl.DisplayNames([locale], { type: 'language' })
  const localizedLang = languageNames[locale].of(code)
  if (localizedLang && localizedLang !== code) return localizedLang

  // fallback to hardcoded lang codes
  if (code in langCodes) return (langCodes as any)[code]

  // fallback 2: no name found -> just the code
  return code
}

export function getAllLanguageCodes(): string[] {
  return Object.keys(langCodes)
}

export function isLanguageCodeValid(code: string) {
  return !!(langCodes as any)[code]
}
