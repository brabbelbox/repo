/* eslint-disable import/prefer-default-export */

/**
 * https://stackoverflow.com/a/4819886
 */
export function isTouchDevice() {
  return (('ontouchstart' in window)
     || (navigator.maxTouchPoints > 0)
     || ((navigator as any).msMaxTouchPoints > 0))
}
