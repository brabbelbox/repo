import fs from 'fs'

// https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry
const data = fs.readFileSync('./language-subtag-registry', 'utf-8')

const entries = data.split("\n%%\n")


const output = {}

const signLanguages: string[] = []

for(let entry of entries) {
  const e = {}
  for(let line of entry.split('\n')) {
    const [key, value] = line.split(': ')
    if(e[key]) {
      e[key] = e[key] + ' / ' + value
    } else {
      e[key] = value
    }
  }

  if(e['Type'] === 'extlang' && e['Prefix'] === 'sgn') {
    // we are working with audio, so sign languages
    // need to be handled in other ways
    signLanguages.push(e['Subtag'] as string)
    if(output[e['Subtag']]) delete output[e['Subtag']]
  } else {


      if(e['Type'] !== 'language') continue
      if(e['Deprecated']) continue
      if(e['Scope'] && e['Scope'] !== 'macrolanguage') continue

      // console.log(e)
      output[e['Subtag']] = e['Description']
  }
}


console.log(JSON.stringify(output, null, 2))
