# Brabbelbox

## Development

Install dependencies with `yarn` and then run

```
yarn dev
```

### i18n

This project uses `vue-i18n` for translation. Consult [the docs](https://vue-i18n.intlify.dev/guide/) for usage instructions.

## Build

```
yarn build
```
