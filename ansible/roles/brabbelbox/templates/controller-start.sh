#!/bin/sh
cd /opt/brabbelbox/controller
deno run \
  --import-map vendor/import_map.json \
  --allow-net --allow-read --allow-write --allow-run --allow-env \
  src/server.ts
