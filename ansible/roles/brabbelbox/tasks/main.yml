- name: Add pipewire repository
  ansible.builtin.apt_repository:
    repo: ppa:pipewire-debian/pipewire-upstream

- name: install packages
  apt:
    pkg:
      - pipewire
      - pipewire-jack
      - qpwgraph
      - nginx
      - rsync
      - libjack-dev
      - pipewire-audio-client-libraries
      - xvfb
      - gnuradio
      - gr-osmosdr
      - hackrf

# - name: add XDG_RUNTIME_DIR to ~/.profile
#   lineinfile:
#     line: export XDG_RUNTIME_DIR="/run/user/$UID"
#     path: /home/{{ ansible_user }}/.profile

- name: create /opt/brabbelbox directories
  file:
    path: "{{ item }}"
    state: directory
    owner: "{{ ansible_user }}"
    group: "{{ ansible_user }}"
  with_items:
    - /opt/brabbelbox
    - /opt/brabbelbox/controller
    - /opt/brabbelbox/controller/presets
    - /opt/brabbelbox/bin

- name: controller config.yaml
  template:
    src: config.yaml
    dest: /opt/brabbelbox/controller/config.yaml
    owner: "{{ ansible_user }}"
    group: "{{ ansible_user }}"

- name: controller start script
  template:
    src: controller-start.sh
    dest: /opt/brabbelbox/controller/start.sh
    owner: "{{ ansible_user }}"
    group: "{{ ansible_user }}"
    mode: "+x"

- name: add mixer service
  template:
    src: brabbelbox-mixer.service
    dest: /etc/systemd/user/brabbelbox-mixer.service
  notify:
      - start brabbelbox-mixer

- name: add plumber service
  template:
    src: brabbelbox-plumber.service
    dest: /etc/systemd/user/brabbelbox-plumber.service
  notify:
    - systemd daemon reload
    - start brabbelbox-plumber

- name: add controller service
  template:
    src: brabbelbox-controller.service
    dest: /etc/systemd/user/brabbelbox-controller.service
  notify:
    - start brabbelbox-controller

- name: allow nginx to access frontend files
  user:
    name: www-data
    groups: "{{ ansible_user }}"
    append: yes

- name: allow ubuntu user to access sound cards
  user:
    name: "{{ ansible_user }}"
    groups: audio
    append: yes

- name: allow audio group to set realtime scheduling
  copy:
    dest: /etc/security/limits.d/10-audio.conf
    content: "@audio  - rtprio 50"

- name: nginx config
  template:
    src: nginx.conf
    dest: /etc/nginx/sites-enabled/default
  notify:
    - reload nginx

- name: create /etc/systemd/system/nginx.service.d
  file:
    path: /etc/systemd/system/nginx.service.d
    state: directory

- name: create /var/log/nginx on nginx start
  template:
    src: nginx.service.override.conf
    dest: /etc/systemd/system/nginx.service.d/override.conf
  notify:
    - reload nginx

- name: enable services
  become: no
  systemd:
    name: "{{ item }}"
    enabled: yes
    scope: user
  with_items:
    - brabbelbox-mixer.service
    - brabbelbox-controller.service
    - brabbelbox-plumber.service

- name: enable systemd user services on boot
  ansible.builtin.command: loginctl enable-linger ubuntu
  args:
    creates: /var/lib/systemd/linger/ubuntu
